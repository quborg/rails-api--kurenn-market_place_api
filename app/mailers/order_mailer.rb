class OrderMailer < ActionMailer::Base

	default from: "no-reply@zamin.tron"

	def send_confirmation(order)
		@order = order
		@user = @order.user
		mail to: @user.email, subject: "Order Confirmation"
	end

end
