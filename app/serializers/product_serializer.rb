class ProductSerializer < ActiveModel::Serializer
 	attributes :id, :title, :sale_price, :published
 	has_one :user

 	def cache_key
		[object, scope]
	end
end
