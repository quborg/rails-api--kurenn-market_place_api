class CreatePlacements < ActiveRecord::Migration
  def change
    create_table :placements do |t|
      t.references	:order,		index: true, foreign_key: true
      t.references	:product,	index: true, foreign_key: true
      t.integer		:quantity,	default: 0

      t.timestamps null: false
    end
  end
end
